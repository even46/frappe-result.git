<?php
// +----------------------------------------------------------------------
// | ApiResult
// +----------------------------------------------------------------------
// | 日期 2024-03-28
// +----------------------------------------------------------------------
// | 开发者 Even <even@1000duo.cn>
// +----------------------------------------------------------------------
// | 版权所有 2024~2025 苏州千朵网络科技有限公司 [ https://www.1000duo.cn ]
// +----------------------------------------------------------------------

namespace frappe\result;

use think\response\Json;

class ApiResult
{
    /**
     * 成功
     * @param mixed|null $data
     * @param string $msg
     * @param int $code
     * @return Json
     * @author yinxu
     * @date 2024/3/28 20:38:28
     */
    public static function success($data = null, string $msg = "ok", int $code = 200): Json
    {
        $response = [
            'code' => $code,
            'msg' => $msg,
        ];
        if (!is_null($data)) $response['data'] = $data;
        if (env('APP_DEBUG', false)) {
            $response['debug'] = request();
        }
        return json($response);
    }

    /**
     * 失败
     * @param \Throwable|string $msg
     * @param int $code
     * @return Json
     * @author yinxu
     * @date 2024/3/28 20:39:08
     */
    public static function error($msg = 'fail', int $code = 0): Json
    {
        $response = [
            'code' => $code,
            'msg' => $msg,
        ];
        if ($msg instanceof \Throwable) {
            $response['code'] = $msg->getCode();
            $response['msg'] = $msg->getMessage();
        }
        if (env('APP_DEBUG', false)) {
            $response['debug'] = request();
        }
        return json($response);
    }
}




