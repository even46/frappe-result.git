
# frappe-result

thinkphp6｜thinkphp8 API Result 类库

## 安装
> composer require yinxu46/frappe-result



## 使用

### 助手函数

失败
~~~
public function success()
{
    return api_success($data = null, string $msg = "ok", int $code = 200);
}
~~~

成功
~~~
public function error()
{
    return api_error($msg = "fail", int $code = 200);
}
~~~